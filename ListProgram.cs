using System;
using System.Collections.Generic;


class Customer : IEquatable<Customer>, IComparable<Customer>
{
	public Customer()
	{
		// nothing
	}
	
	public Customer(int cid, string fname, string lname, int age, string addr)
	{
		CustID = cid;
		FirstName = fname;
		LastName = lname;
		Age = age;
		Address = addr;
	}
	
	public static int SortOption { get; set; }
	public int CustID { get; set; }
	public string FirstName { get; set; }
	public string LastName { get; set; }
	public int Age { get; set; }
	public string Address { get; set; }

	public override string ToString()
	{
		return "---------------\nCustomer ID : " + CustID + "\nName: " + LastName + ", " + FirstName + "\nAge:" + Age + "\nAddress: " + Address + "\n---------------";
	}

	public override bool Equals( object obj )
	{
		if (obj == null) return false;
		Customer cust = obj as Customer;
		if (cust == null) return false;
		else return Equals( cust );
	}
	
	public override int GetHashCode()
	{
		return CustID;
	}
	
	public bool Equals(Customer other)
	{
		if (other == null) return false;
		return (this.CustID.Equals(other.CustID));
	}

	public int CompareTo(Customer compareCustomer)
	{
		if (compareCustomer == null)
				return 1;
		switch( SortOption )
		{
			case 1:
				return this.CustID.CompareTo(compareCustomer.CustID);
			case 2:
				return this.FirstName.CompareTo(compareCustomer.FirstName);
			case 3:
				return this.LastName.CompareTo(compareCustomer.LastName);
			case 4:
				return this.Age.CompareTo(compareCustomer.Age);
			case 5:
				return this.Address.CompareTo(compareCustomer.Address);
			default : 
				return 0;
		}
	}
}

class ListProgram
{
	public static void Main()
	{
		List<Customer> CustomerList = new List<Customer>();
		CustomerList.Add(new Customer(101, "abc", "bcd", 21, "DLF"));
		CustomerList.Add(new Customer(201, "ijk", "jkl", 21, "ALD"));
		CustomerList.Add(new Customer(102, "cde", "def", 22, "DLF"));
		
		char option;
		int cid, age;
		string fname, lname, addr, temp;
		
		do
		{
			Console.WriteLine("Enter your choice:\n1 : Add a customer.\n2 : Remove a customer.\n3 : Search for a customer.\n4 : List all customers.\n5 : Sort the customer list.\n6 : EXIT");
			temp = Console.ReadLine();
			option = (temp.Length < 1) ? 'n' : temp[0];
			switch (option)
			{
				case '1':
					Console.Write("Enter the customer ID : ");
					cid = Convert.ToInt32(Console.ReadLine());
					Console.Write("Enter the customer's first name : ");
					fname = Console.ReadLine();
					Console.Write("Enter the customer's last name : ");
					lname = Console.ReadLine();
					Console.Write("Enter the customer's age : ");
					age = Convert.ToInt32(Console.ReadLine());
					Console.Write("Enter the customer's address : ");
					addr = Console.ReadLine();
					CustomerList.Add(new Customer(cid, fname, lname, age, addr));
					break;
					
				case '2':
					Console.Write("Enter the ID of the customer to be deleted : ");
					cid = Convert.ToInt32(Console.ReadLine());
					CustomerList.Remove( new Customer() { CustID = cid } );
					break;
					
				case '3':
					Console.Write("Enter the ID of the customer to be searched : ");
					cid = Convert.ToInt32(Console.ReadLine());
					if(CustomerList.Contains(new Customer {CustID = cid}))
					{
						foreach( Customer cust in CustomerList )
						{
							if( cust.CustID == cid )
							{
								Console.WriteLine(cust.ToString() + " found.");
							}
						} 
					}
					else
					{
						Console.WriteLine("Customer with ID " + cid + " not found in the record.");
					}
					break;
				case '4':
					foreach( Customer cust in CustomerList )
					{
						Console.WriteLine(cust.ToString());
					}
					break;
				case '5':
					Console.WriteLine("Select the parameter to sort the Customer Collection\n1 : Customer ID\n2 : First Name\n3 : Last Name\n4 : Age\n5 : Address");
					int k = Convert.ToInt32(Console.ReadLine());
					Customer.SortOption = (int) k;
					CustomerList.Sort();
					break;
				case '6':
					break;
				default :
					Console.WriteLine("-.- Not a valid choice! -.-");
					break;
			}
		} while( option != '6' );
	}
}