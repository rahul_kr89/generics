﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace generic_exmple
{
    public abstract class CustomerBase
    {
        protected Guid? _UID;
		public static int comparisionParameter = 0;
        public CustomerBase()
        {
            _UID = Guid.NewGuid();
        }

        public Guid? UID
        {
            get
            {
                return _UID;
            }
            set
            {
                _UID = value;
            }
        }
    }

    public class Customer : CustomerBase, IComparable
    {
        private int _CustomerID = 0;
        private string _FirstName = "";
        private string _LastName = "";
        private int _Age = 0;
        private string _Address = "";

        public Customer()
        { }

        public Customer(int CID, string fname, string lname, int age, string addr)
        {
            _CustomerID = CID;
            _FirstName = fname;
            _LastName = lname;
            _Age = age;
            _Address = addr;
        }

        public int CustomerID
        {
            get
            {
                return _CustomerID;
            }
            set
            {
                _CustomerID = value;
            }
        }

        public string FirstName
        {
            get
            {
                return _FirstName;
            }
            set
            {
                _FirstName = value;
            }
        }

        public string LastName
        {
            get
            {
                return _LastName;
            }
            set
            {
                _LastName = value;
            }
        }

        public int Age
        {
            get
            {
                return _Age;
            }
            set
            {
                _Age = value;
            }
        }

        public string Address
        {
            get
            {
                return _Address;
            }
            set
            {
                _Address = value;
            }
        }

        public override string ToString()
        {
            return "---------------\nCustomer ID : " + CustomerID.ToString() + "\nFirst name : " + FirstName + "\nLast name : " + LastName + "\nAge : " + Age.ToString() + "\nAddress : " + Address + "\n---------------";
        }

        public int CompareTo(object obj)
        {
            Customer cust = obj as Customer;
			switch( CustomerBase.comparisionParameter )
			{
				default:
				case 1:
					if (cust.CustomerID < CustomerID)
					{
						return 1;
					}
					if (cust.CustomerID > CustomerID)
					{
						return -1;
					}
					return 0;
				case 2:
					return string.Compare(FirstName, cust.FirstName, true);
				case 3:
					return string.Compare(LastName, cust.LastName, true);
				case 4:
					if (cust.Age < Age)
					{
						return 1;
					}
					if (cust.Age > Age)
					{
						return -1;
					}
					return 0;
				case 5:
		            return string.Compare(Address, cust.Address, true);
			}
        }
    }

    public class CustomerCollection<T> : ICollection<T> where T : CustomerBase
    {
        protected ArrayList _innerArray;
        protected bool _IsReadOnly;

        public CustomerCollection()
        {
            _innerArray = new ArrayList();
        }

        public T this[int index]
        {
            get
            {
                return (T)_innerArray[index];
            }
            set
            {
                _innerArray[index] = value;
            }
        }

        public virtual int Count
        {
            get
            {
                return _innerArray.Count;
            }
        }

        public virtual bool IsReadOnly
        {
            get
            {
                return _IsReadOnly;
            }
        }

        public virtual void Add(T CustomerObject)
        {
            _innerArray.Add(CustomerObject);
        }

        public virtual bool Remove(T CustomerObject)
        {
            int count = _innerArray.Count;
            for (int i = 0; i < count; i++)
            {
                T obj = (T)_innerArray[i];
                if (obj.UID == CustomerObject.UID)
                {
                    _innerArray.RemoveAt(i);
                    return true;
                }
            }
            return false;
        }

        public bool Contains(T CustomerObject)
        {
            foreach (T obj in _innerArray)
            {
                if (obj.UID == CustomerObject.UID)
                {
                    return true;
                }
            }
            return false;
        }

        public virtual void CopyTo(T[] CustomerArray, int index)
        {
            throw new Exception();
        }

        public virtual void Clear()
        {
            _innerArray.Clear();
        }

        public void ListAll()
        {
            foreach (T obj in _innerArray)
            {
                obj.ToString();
            }
        }

        public void Sort( int param )
        {
			CustomerBase.comparisionParameter = param;
            _innerArray.Sort();
        }

        public virtual IEnumerator<T> GetEnumerator()
        {
            return new CustomerEnumerator<T>(this);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new CustomerEnumerator<T>(this);
        }
    }

    public class CustomerEnumerator<T> : IEnumerator<T> where T : CustomerBase
    {
        protected CustomerCollection<T> _collection;
        protected int index;
        protected T _current;

        public CustomerEnumerator()
        { }

        public CustomerEnumerator(CustomerCollection<T> collection)
        {
            _collection = collection;
            index = -1;
            _current = default(T);
        }

        public virtual T Current
        {
            get
            {
                return _current;
            }
        }

        object IEnumerator.Current
        {
            get
            {
                return _current;
            }
        }

        public virtual void Dispose()
        {
            _collection = null;
            _current = default(T);
            index = -1;
        }

        public virtual bool MoveNext()
        {
            if (++index >= _collection.Count)
            {
                return false;
            }
            else
            {
                _current = _collection[index];
            }
            return true;
        }

        public virtual void Reset()
        {
            _current = default(T);
            index = -1;
        }
    }

    public  class ArrayListProgram
    {
        private static void Main(string[] args)
        {
            Customer cust1 = new Customer(101, "abc", "bcd", 21, "DLF");
            Customer cust2 = new Customer(102, "cde", "def", 22, "DLF");
            //Customer cust3 = new Customer(103, "efg", "fgh", 21, "DLF");
            //Customer cust4 = new Customer(104, "ghi", "hij", 21, "DLF");
            Customer cust5 = new Customer(201, "ijk", "jkl", 21, "DLF");
            //Customer cust6 = new Customer(202, "klm", "lmn", 21, "DLF");
            //Customer cust7 = new Customer(203, "mno", "nop", 21, "DLF");
            //Customer cust8 = new Customer(301, "opq", "pqr", 21, "DLF");

            CustomerCollection<Customer> collection = new CustomerCollection<Customer>();
            collection.Add(cust1);
            collection.Add(cust2);
            //collection.Add(cust3);
            //collection.Add(cust4);
            collection.Add(cust5);
            //collection.Add(cust6);
            //collection.Add(cust7);
            //collection.Add(cust8);

            bool found;
            char option;
            int cid, age;
            string fname, lname, addr, temp;
            do
            {
                Console.WriteLine("Enter your choice:\n1 : Add a customer.\n2 : Remove a customer.\n3 : Search for a customer.\n4 : List all customers.\n5 : Sort the customer list.\n6 : EXIT");
                temp = Console.ReadLine();
				option = (temp.Length < 1) ? 'n' : temp[0];
                switch (option)
                {
                    case '1':
                        Console.Write("Enter the customer ID : ");
                        cid = Convert.ToInt32(Console.ReadLine());
                        Console.Write("Enter the customer's first name : ");
                        fname = Console.ReadLine();
                        Console.Write("Enter the customer's last name : ");
                        lname = Console.ReadLine();
                        Console.Write("Enter the customer's age : ");
                        age = Convert.ToInt32(Console.ReadLine());
                        Console.Write("Enter the customer's address : ");
                        addr = Console.ReadLine();
                        collection.Add(new Customer(cid, fname, lname, age, addr));
                        break;
                    case '2':
                        Console.Write("Enter the ID of the customer to be removed : ");
                        cid = Convert.ToInt32(Console.ReadLine());
                        foreach (Customer cust in collection)
                        {
                            if (cust.CustomerID == cid)
                            {
                                collection.Remove(cust);
                                Console.WriteLine("Customer with ID " + cid.ToString() + " successfully removed.");
                            }
                        }
                        break;
                    case '3':
                        Console.Write("Enter the ID of the customer to be searched : ");
                        cid = Convert.ToInt32(Console.ReadLine());
                        foreach (Customer cust in collection)
                        {
                            if (cust.CustomerID == cid)
                            {
                                found = collection.Contains(cust);
                                if (found)
                                {
									Console.WriteLine(cust.ToString());
                                    Console.WriteLine("Customer with ID " + cid.ToString() + " found.");
                                }
                            }
                        }
                        break;
                    case '4':
                        //collection.ListAll();
                        foreach(Customer cust in collection)
                        {
                            Console.WriteLine(cust.ToString());
                        }
                        break;
                    case '5':
						Console.WriteLine("Select the parameter to sort the Customer Collection\n1 : Customer ID\n2 : First Name\n3 : Last Name\n4 : Age\n5 : Address");
						int k = Convert.ToInt32(Console.ReadLine());
                        collection.Sort(k);
                        break;
					case '6':
						break;
                    default:
                        Console.WriteLine("Not a valid choice");
                        break;
                }
            } while( option != '6' );
        }
    }
}
