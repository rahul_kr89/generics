//RAHUL KUMAR

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using generic_exmple;

namespace EventHandling
{
    class Class1
    {
        static void SizeChanged(object source, MyEventArgs e)
        {
            Console.WriteLine("The size of the arraylist has been altered!");
        }

        static void abc(object src, MyEventArgs e)
        {
            Console.WriteLine("The new Customer added is: " );
            Console.WriteLine(e.CustomerCollectionObject.ToString());
        }

        private static void Main(string[] args)
        {
            Customer cust1 = new Customer(101, "abc", "bcd", 21, "DLF");
            Customer cust2 = new Customer(102, "cde", "def", 22, "DLF");
            //Customer cust3 = new Customer(103, "efg", "fgh", 21, "DLF");
            //Customer cust4 = new Customer(104, "ghi", "hij", 21, "DLF");
            Customer cust5 = new Customer(201, "ijk", "jkl", 21, "DLF");
            //Customer cust6 = new Customer(202, "klm", "lmn", 21, "DLF");
            //Customer cust7 = new Customer(203, "mno", "nop", 21, "DLF");
            //Customer cust8 = new Customer(301, "opq", "pqr", 21, "DLF");

            CustomerCollection<Customer> collection = new CustomerCollection<Customer>();
            collection.OnSizeChange += new MyEventHandler(SizeChanged);
            collection.OnSizeChange += new MyEventHandler(abc);
            collection.Add(cust1);
            collection.Add(cust2);
            //collection.Add(cust3);
            //collection.Add(cust4);
            collection.Add(cust5);
            //collection.Add(cust6);
            //collection.Add(cust7);
            //collection.Add(cust8);

            bool found;
            char option;
            int cid, age;
            string fname, lname, addr, temp;
            do
            {
                Console.WriteLine("Enter your choice:\n1 : Add a customer.\n2 : Remove a customer.\n3 : Search for a customer.\n4 : List all customers.\n5 : Sort the customer list.\n6 : EXIT");
                temp = Console.ReadLine();
                option = (temp.Length < 1) ? 'n' : temp[0];
                switch (option)
                {
                    case '1':
                        Console.Write("Enter the customer ID : ");
                        cid = Convert.ToInt32(Console.ReadLine());
                        Console.Write("Enter the customer's first name : ");
                        fname = Console.ReadLine();
                        Console.Write("Enter the customer's last name : ");
                        lname = Console.ReadLine();
                        Console.Write("Enter the customer's age : ");
                        age = Convert.ToInt32(Console.ReadLine());
                        Console.Write("Enter the customer's address : ");
                        addr = Console.ReadLine();
                        collection.Add(new Customer(cid, fname, lname, age, addr));
                        break;
                    case '2':
                        Console.Write("Enter the ID of the customer to be removed : ");
                        cid = Convert.ToInt32(Console.ReadLine());
                        foreach (Customer cust in collection)
                        {
                            if (cust.CustomerID == cid)
                            {
                                collection.Remove(cust);
                                Console.WriteLine("Customer with ID " + cid.ToString() + " successfully removed.");
                            }
                        }
                        break;
                    case '3':
                        Console.Write("Enter the ID of the customer to be searched : ");
                        cid = Convert.ToInt32(Console.ReadLine());
                        foreach (Customer cust in collection)
                        {
                            if (cust.CustomerID == cid)
                            {
                                found = collection.Contains(cust);
                                if (found)
                                {
                                    Console.WriteLine(cust.ToString());
                                    Console.WriteLine("Customer with ID " + cid.ToString() + " found.");
                                }
                            }
                        }
                        break;
                    case '4':
                        //collection.ListAll();
                        foreach (Customer cust in collection)
                        {
                            Console.WriteLine(cust.ToString());
                        }
                        break;
                    case '5':
                        Console.WriteLine("Select the parameter to sort the Customer Collection\n1 : Customer ID\n2 : First Name\n3 : Last Name\n4 : Age\n5 : Address");
                        int k = Convert.ToInt32(Console.ReadLine());
                        collection.Sort(k);
                        break;
                    case '6':
                        break;
                    default:
                        Console.WriteLine("Not a valid choice");
                        break;
                }
            } while (option != '6');
        }
    }
}
