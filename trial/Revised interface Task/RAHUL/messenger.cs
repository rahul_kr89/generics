//RAHUL KUMAR

using System;
using Interface;

namespace msngr {
	public class interfaceInherit:IMessage {
		
		char sender = 'Y'; // Y: YOU; O: OTHER
		
		string IMessage.Send() {
			string msg = Console.ReadLine();
			return msg;
		}
		
		void IMessage.Receive(string msg) {
			if( msg.Length > 0 ) {
				switch( sender ) {
					case 'Y':
						Console.WriteLine("YOU: " + msg);
						sender = 'O';
						break;
					case 'O':
						Console.WriteLine("OTHER: " + msg);
						sender = 'Y';
						break;
					default:
						break;
				}
			}
		}
		
	};

	public class messenger {
		public static void Main(string[] args) {
			IMessage obj = new interfaceInherit();
			string message = "abc";
			Console.WriteLine(DateTime.Now);
			while( message.Length > 0 ) {
				message = obj.Send();
				obj.Receive(message);
			}
		}
	};
}